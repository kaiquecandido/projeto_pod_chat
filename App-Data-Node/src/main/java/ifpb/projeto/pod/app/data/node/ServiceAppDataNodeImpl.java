/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.app.data.node;

import ifpb.projeto.pod.app.data.node.dropbox.ServiceDropBoxImpl;
import ifpb.projeto.pod.app.data.node.dropbox.ServiceDropBox;
import ifpb.projeto.pod.app.data.node.googleDrive.ServiceGoogleDrive;
import ifpb.projeto.pod.app.data.node.googleDrive.ServiceGoogleDriveImpl;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kaique
 */
public class ServiceAppDataNodeImpl extends UnicastRemoteObject implements ServiceAppDataNode {

    public ServiceAppDataNodeImpl() throws RemoteException {
        super();
    }

    @Override
    public boolean registroDeUsuario(String email, String senha) throws RemoteException {
        ServiceDropBox serviceDropBox = new ServiceDropBoxImpl();
        return serviceDropBox.registroDeUsuario(email, senha);
    }

    @Override
    public String loginDeUsuario(String email, String senha) throws RemoteException {
        ServiceDropBox serviceDropBox = new ServiceDropBoxImpl();
        return serviceDropBox.loginDeUsuario(email, senha);
    }

    @Override
    public boolean envioDeMensagem(String mensagem) throws RemoteException {
        ServiceGoogleDrive serviceGoogleDrive = new ServiceGoogleDriveImpl();
        return serviceGoogleDrive.envioDeMensagem(mensagem);
    }

    @Override
    public List<String> todosOsGrupos() throws RemoteException {
        try {
            FileReader reader = new FileReader("./Grupos/Grupos.txt");
            BufferedReader leitor = new BufferedReader(reader);
            List<String> listaDeGrupos = new ArrayList<String>();
            while (true) {
                String linha = leitor.readLine();
                if (linha == null) {
                    break;
                } else {
                    listaDeGrupos.add(linha);
                }
            }
            return listaDeGrupos;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServiceAppDataNodeImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ServiceAppDataNodeImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
