/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.app.data.node.dropbox;

/**
 *
 * @author kaique
 */
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceDropBoxImpl extends UnicastRemoteObject implements ServiceDropBox {

    public ServiceDropBoxImpl() throws RemoteException {
        super();
    }

    private static final String ACCESS_TOKEN = "kPbz7ywwrZAAAAAAAAABAaXIO87mJt6W6TB2FPl1ooNQyp4aijzlbXpj8hjFfjCz";

    @Override
    public boolean registroDeUsuario(String email, String senha) throws RemoteException {
        try {
            // Criando um cliente Dropbox
            DbxRequestConfig config = new DbxRequestConfig("dropbox/java-tutorial", "en_US");
            DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);

            // Download do arquivo que contem os usurios.
            FileMetadata usuarios = client.files().download("/POD/Usuarios.txt").download(new FileOutputStream("./Usuarios/Usuarios.txt"));

            // Escrevendo no arquivo baixado            
            FileWriter fileWriter = new FileWriter("./Usuarios/Usuarios.txt", true);
            BufferedWriter bw = new BufferedWriter(fileWriter);
            bw.newLine();
            bw.write(email + ";" + senha);
            bw.close();
            fileWriter.close();

            // Deletando arquivo do DropBox Para fazer um novo Upload
            client.files().delete("/POD/Usuarios.txt");

            // Fazendo Upload do arquivo que foi alterado                               
            InputStream in = new FileInputStream("./Usuarios/Usuarios.txt");
            FileMetadata metadata = client.files().uploadBuilder("/POD/Usuarios.txt").uploadAndFinish(in);
            return true;

        } catch (IOException | DbxException ex) {
            Logger.getLogger(ServiceDropBoxImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    @Override
    public String loginDeUsuario(String email, String senha) throws RemoteException {
        try {
            // Criando um cliente Dropbox
            DbxRequestConfig config = new DbxRequestConfig("dropbox/java-tutorial", "en_US");
            DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);

            // Download do arquivo que contem os usurios.
            FileMetadata usuarios = client.files().download("/POD/Usuarios.txt").download(new FileOutputStream("./Usuarios/Usuarios.txt"));

            // Lendo arquivo Usuarios.txt                        
            FileReader reader = new FileReader("./Usuarios/Usuarios.txt");
            //leitor do arquivo
            BufferedReader leitor = new BufferedReader(reader);
            while (true) {
                String linha = leitor.readLine();
                if (linha == null) {
                    break;
                } else {
                    // Verifica se o Login e Senha informados batem com algum do arquito txt
                    String[] split = linha.split(";");
                    boolean login = split[0].equalsIgnoreCase(email);
                    boolean password = split[1].equalsIgnoreCase(senha);
                    if (login == true && password == true) {
                        return linha;
                    }
                }
            }
            return null;
        } catch (IOException | DbxException ex) {
            Logger.getLogger(ServiceDropBoxImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
