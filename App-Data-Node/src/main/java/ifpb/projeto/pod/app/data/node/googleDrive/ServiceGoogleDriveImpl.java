/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.app.data.node.googleDrive;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kaique
 */
public class ServiceGoogleDriveImpl extends UnicastRemoteObject implements ServiceGoogleDrive {

    public ServiceGoogleDriveImpl() throws RemoteException {
        super();
    }

    @Override
    public boolean envioDeMensagem(String mensagem) throws RemoteException {
        try {
            String[] split = mensagem.split(";");
            String id = split[0];
            String emailUsuario = split[1];
            String dataEnvio = split[2];
            String msg = split[3];
            String idGrupo = split[4];

            if (idGrupo.contains("0")) {
                System.out.println("Equals igual a 0");
                FileWriter fileWriter = new FileWriter("./Mensagens/Mensagens.txt", true);
                BufferedWriter bw = new BufferedWriter(fileWriter);
                bw.newLine();
                bw.write(id + ";" + emailUsuario + ";" + dataEnvio + ";" + msg + ";" + idGrupo);

                FileWriter fileWriterGrupo = new FileWriter("./Grupos/POD.txt", true);
                BufferedWriter bwGrupo = new BufferedWriter(fileWriterGrupo);
                bwGrupo.newLine();
                bwGrupo.write(id + ";" + emailUsuario + ";" + dataEnvio + ";" + msg + ";" + idGrupo);

                bw.close();
                bwGrupo.close();
                fileWriter.close();
                fileWriterGrupo.close();
                System.out.println("Terminou de escrever");
                return true;
            } else if (idGrupo.contains("1")) {
                System.out.println("Equals igual a 1");
                FileWriter fileWriter = new FileWriter("./Mensagens/Mensagens.txt", true);
                BufferedWriter bw = new BufferedWriter(fileWriter);
                bw.newLine();
                bw.write(id + ";" + emailUsuario + ";" + dataEnvio + ";" + msg + ";" + idGrupo);
                
                FileWriter fileWriterGrupo = new FileWriter("./Grupos/DAC.txt", true);
                BufferedWriter bwGrupo = new BufferedWriter(fileWriterGrupo);
                bwGrupo.newLine();
                bwGrupo.write(id + ";" + emailUsuario + ";" + dataEnvio + ";" + msg + ";" + idGrupo);

                bw.close();
                bwGrupo.close();
                fileWriter.close();
                fileWriterGrupo.close();
                System.out.println("Terminou de escrever");
                return true;
            } else if (idGrupo.contains("2")) {
                System.out.println("Equals igual a 2");
                FileWriter fileWriter = new FileWriter("./Mensagens/Mensagens.txt", true);
                BufferedWriter bw = new BufferedWriter(fileWriter);
                bw.newLine();
                bw.write(id + ";" + emailUsuario + ";" + dataEnvio + ";" + msg + ";" + idGrupo);
                
                FileWriter fileWriterGrupo = new FileWriter("./Grupos/PRATICAS.txt", true);
                BufferedWriter bwGrupo = new BufferedWriter(fileWriterGrupo);
                bwGrupo.newLine();
                bwGrupo.write(id + ";" + emailUsuario + ";" + dataEnvio + ";" + msg + ";" + idGrupo);

                bw.close();
                bwGrupo.close();
                fileWriter.close();
                fileWriterGrupo.close();
                System.out.println("Terminou de escrever");
                return true;
            } else if (idGrupo.contains("3")) {
                System.out.println("Equals igual a 3");
                FileWriter fileWriter = new FileWriter("./Mensagens/Mensagens.txt", true);
                BufferedWriter bw = new BufferedWriter(fileWriter);
                bw.newLine();
                bw.write(id + ";" + emailUsuario + ";" + dataEnvio + ";" + msg + ";" + idGrupo);
                
                FileWriter fileWriterGrupo = new FileWriter("./Grupos/ESTUDOS.txt", true);
                BufferedWriter bwGrupo = new BufferedWriter(fileWriterGrupo);
                bwGrupo.newLine();
                bwGrupo.write(id + ";" + emailUsuario + ";" + dataEnvio + ";" + msg + ";" + idGrupo);

                bw.close();
                bwGrupo.close();
                fileWriter.close();
                fileWriterGrupo.close();
                System.out.println("Terminou de escrever");
                return true;
            }else{
                System.out.println("Não pegou nenhum idGrupo = " + idGrupo);
            }
        } catch (IOException ex) {
            Logger.getLogger(ServiceGoogleDriveImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
