/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.app.data.node;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author kaique
 */
public interface ServiceAppDataNode extends Remote {

    boolean registroDeUsuario(String email, String senha) throws RemoteException;

    String loginDeUsuario(String email, String senha) throws RemoteException;
    
    boolean envioDeMensagem(String mensagem) throws RemoteException;
    
    List<String> todosOsGrupos() throws RemoteException;
}
