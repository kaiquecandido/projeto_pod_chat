/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.app.data.node.dropbox;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author kaique
 */
public interface ServiceDropBox extends Remote{

    boolean registroDeUsuario(String email, String senha) throws RemoteException;
    
    String loginDeUsuario(String email, String senha) throws RemoteException;
}
