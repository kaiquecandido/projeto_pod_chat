/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.app.data.node.googleDrive;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author kaique
 */
public interface ServiceGoogleDrive extends Remote{
    
    boolean envioDeMensagem(String mensagem) throws RemoteException;
    
}
