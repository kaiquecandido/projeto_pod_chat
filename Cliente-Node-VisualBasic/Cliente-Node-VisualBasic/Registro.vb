﻿Imports System.Net.Sockets

Public Class Registro

    Private Sub registrar_Click(sender As Object, e As EventArgs) Handles registrar.Click

        Dim clientSocket As New TcpClient
        Dim networkstream As NetworkStream

        clientSocket.Connect("localhost", 8083)
        networkstream = clientSocket.GetStream()

        Dim outStream As Byte() = System.Text.Encoding.ASCII.GetBytes(2 & ";" & emailCadastro.Text & ";" & senhaCadastro.Text)
        networkstream.Write(outStream, 0, outStream.Length)
        networkstream.Flush()

        Dim InStream(1024) As Byte
        networkstream.Read(InStream, 0, 1024)
        Dim respostaServidor As String = System.Text.Encoding.ASCII.GetString(InStream)

        Dim testCheck As Boolean
        testCheck = respostaServidor Like "true"

        If respostaServidor.Length() > 2 Then
            MsgBox("Registro realizado com sucesso!", MsgBoxStyle.Information, "Registro Realizado")
            Me.Hide()
            Login.Show()
        Else
            MsgBox("Registro Falhou!", MsgBoxStyle.Information, "Registro Falhou")
        End If
    End Sub

    Private Sub cancelar_Click(sender As Object, e As EventArgs) Handles cancelar.Click
        Me.Hide()
        Login.Show()
    End Sub
End Class