﻿Imports System.Net.Sockets

Public Class Chat
    Public Sub New(ByVal usuario As String)
        InitializeComponent()
        emailUser.Text = usuario
    End Sub
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim caminho As String = "D:/IFPB/5.Periodo/POD/Projeto_Chat/App-Data-Node/Grupos/Grupos.txt"
        If IO.File.Exists(caminho) = True Then
            Dim objreader As New System.IO.StreamReader(caminho)
            While (objreader.EndOfStream() = False)
                ListGroups.Items.Add(objreader.ReadLine)
            End While
            objreader.Close()
        Else
            MessageBox.Show("Esse diretorio não existe!")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim clientSocket As New TcpClient
        Dim networkstream As NetworkStream

        clientSocket.Connect("localhost", 8083)
        networkstream = clientSocket.GetStream()

        Dim outStream As Byte() = System.Text.Encoding.ASCII.GetBytes(3 & ";" & mensagemChat.Text & ";" & emailUser.Text & ";" & ListGroups.SelectedIndex())
        networkstream.Write(outStream, 0, outStream.Length)
        networkstream.Flush()

        Dim InStream(1024) As Byte
        networkstream.Read(InStream, 0, 1024)
        Dim respostaServidor As Boolean = System.Text.Encoding.ASCII.GetString(InStream)

    End Sub

    Private Sub ListGroups_MouseClick(sender As Object, e As MouseEventArgs) Handles ListGroups.MouseClick
        Dim opcao As Integer = ListGroups.SelectedIndex()
        If opcao = 0 Then
            Dim caminho As String = "D:/IFPB/5.Periodo/POD/Projeto_Chat/App-Data-Node/Grupos/POD.txt"
            If IO.File.Exists(caminho) = True Then
                Dim objreader As New System.IO.StreamReader(caminho)

                caixaDeMensagens.Items.Clear()

                While (objreader.EndOfStream() = False)

                    Dim linha As String = objreader.ReadLine
                    Dim palavras As String() = linha.Split(New Char() {";"})

                    Dim mensagensDoChat As New List(Of String)
                    Dim mensagem As String = palavras(1) & " - " & palavras(3) & " - " & palavras(2)
                    mensagensDoChat.Add(mensagem)

                    Dim aux As String
                    For Each aux In mensagensDoChat
                        caixaDeMensagens.Items.Add(aux)
                    Next
                End While
                objreader.Close()
            Else
                MessageBox.Show("Esse diretorio não existe!")
            End If
        End If
        If opcao = 1 Then
            Dim caminho As String = "D:/IFPB/5.Periodo/POD/Projeto_Chat/App-Data-Node/Grupos/DAC.txt"
            If IO.File.Exists(caminho) = True Then
                Dim objreader As New System.IO.StreamReader(caminho)

                caixaDeMensagens.Items.Clear()

                While (objreader.EndOfStream() = False)

                    Dim linha As String = objreader.ReadLine
                    Dim palavras As String() = linha.Split(New Char() {";"})

                    Dim mensagensDoChat As New List(Of String)
                    Dim mensagem As String = palavras(1) & " - " & palavras(3) & " - " & palavras(2)
                    mensagensDoChat.Add(mensagem)

                    Dim aux As String
                    For Each aux In mensagensDoChat
                        caixaDeMensagens.Items.Add(aux)
                    Next
                End While
                objreader.Close()
            Else
                MessageBox.Show("Esse diretorio não existe!")
            End If
        End If
        If opcao = 2 Then
            Dim caminho As String = "D:/IFPB/5.Periodo/POD/Projeto_Chat/App-Data-Node/Grupos/PRATICAS.txt"
            If IO.File.Exists(caminho) = True Then
                Dim objreader As New System.IO.StreamReader(caminho)

                caixaDeMensagens.Items.Clear()

                While (objreader.EndOfStream() = False)

                    Dim linha As String = objreader.ReadLine
                    Dim palavras As String() = linha.Split(New Char() {";"})

                    Dim mensagensDoChat As New List(Of String)
                    Dim mensagem As String = palavras(1) & " - " & palavras(3) & " - " & palavras(2)
                    mensagensDoChat.Add(mensagem)

                    Dim aux As String
                    For Each aux In mensagensDoChat
                        caixaDeMensagens.Items.Add(aux)
                    Next
                End While
                objreader.Close()
            Else
                MessageBox.Show("Esse diretorio não existe!")
            End If
        End If
        If opcao = 3 Then
            Dim caminho As String = "D:/IFPB/5.Periodo/POD/Projeto_Chat/App-Data-Node/Grupos/ESTUDOS.txt"
            If IO.File.Exists(caminho) = True Then
                Dim objreader As New System.IO.StreamReader(caminho)

                caixaDeMensagens.Items.Clear()

                While (objreader.EndOfStream() = False)

                    Dim linha As String = objreader.ReadLine
                    Dim palavras As String() = linha.Split(New Char() {";"})

                    Dim mensagensDoChat As New List(Of String)
                    Dim mensagem As String = palavras(1) & " - " & palavras(3) & " - " & palavras(2)
                    mensagensDoChat.Add(mensagem)

                    Dim aux As String
                    For Each aux In mensagensDoChat
                        caixaDeMensagens.Items.Add(aux)
                    Next
                End While
                objreader.Close()
            Else
                MessageBox.Show("Esse diretorio não existe!")
            End If
        End If
    End Sub
    Function Split(
        ByVal Expression As String,
        Optional ByVal Delimiter As String = ";",
        Optional ByVal Limit As Integer = -1,
        Optional ByVal Compare As CompareMethod = CompareMethod.Binary
    ) As String()

    End Function

    Private Sub sair_Click(sender As Object, e As EventArgs) Handles sair.Click
        Application.Exit()
    End Sub
End Class

