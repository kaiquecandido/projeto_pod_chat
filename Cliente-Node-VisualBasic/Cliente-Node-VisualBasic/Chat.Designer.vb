﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Chat
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mensagemChat = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListGroups = New System.Windows.Forms.ListBox()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.emailUser = New System.Windows.Forms.Label()
        Me.caixaDeMensagens = New System.Windows.Forms.ListBox()
        Me.sair = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'mensagemChat
        '
        Me.mensagemChat.Location = New System.Drawing.Point(13, 314)
        Me.mensagemChat.Multiline = True
        Me.mensagemChat.Name = "mensagemChat"
        Me.mensagemChat.Size = New System.Drawing.Size(515, 60)
        Me.mensagemChat.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.Location = New System.Drawing.Point(534, 314)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(102, 29)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Enviar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ListGroups
        '
        Me.ListGroups.FormattingEnabled = True
        Me.ListGroups.Location = New System.Drawing.Point(13, 11)
        Me.ListGroups.Name = "ListGroups"
        Me.ListGroups.Size = New System.Drawing.Size(115, 277)
        Me.ListGroups.TabIndex = 3
        '
        'emailUser
        '
        Me.emailUser.AutoSize = True
        Me.emailUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.emailUser.Location = New System.Drawing.Point(12, 295)
        Me.emailUser.Name = "emailUser"
        Me.emailUser.Size = New System.Drawing.Size(49, 16)
        Me.emailUser.TabIndex = 4
        Me.emailUser.Text = "Label1"
        '
        'caixaDeMensagens
        '
        Me.caixaDeMensagens.FormattingEnabled = True
        Me.caixaDeMensagens.Location = New System.Drawing.Point(134, 12)
        Me.caixaDeMensagens.Name = "caixaDeMensagens"
        Me.caixaDeMensagens.Size = New System.Drawing.Size(502, 277)
        Me.caixaDeMensagens.TabIndex = 5
        '
        'sair
        '
        Me.sair.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.sair.Location = New System.Drawing.Point(534, 345)
        Me.sair.Name = "sair"
        Me.sair.Size = New System.Drawing.Size(102, 29)
        Me.sair.TabIndex = 6
        Me.sair.Text = "Sair"
        Me.sair.UseVisualStyleBackColor = True
        '
        'Chat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(648, 386)
        Me.Controls.Add(Me.sair)
        Me.Controls.Add(Me.caixaDeMensagens)
        Me.Controls.Add(Me.emailUser)
        Me.Controls.Add(Me.ListGroups)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.mensagemChat)
        Me.Name = "Chat"
        Me.Text = "Chat"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mensagemChat As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents ListGroups As ListBox
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents emailUser As Label
    Friend WithEvents caixaDeMensagens As ListBox
    Friend WithEvents sair As Button
End Class
