/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.server.node;

import ifpb.projeto.pod.app.data.node.ServiceAppDataNode;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author kaique
 */
public class ServerNodeImpl extends UnicastRemoteObject implements ServerNode {

    public ServerNodeImpl() throws RemoteException {
        super();
    }

    @Override
    public boolean registroDeUsuario(String email, String senha) throws RemoteException {
        return this.lookupAppDataNode().registroDeUsuario(email, senha);
    }

    @Override
    public String loginDeUsuario(String email, String senha) throws RemoteException {
        return this.lookupAppDataNode().loginDeUsuario(email, senha);
    }

    @Override
    public boolean envioDeMensagem(String mensagem) throws RemoteException {
        return this.lookupAppDataNode().envioDeMensagem(mensagem);
    }

    @Override
    public List<String> todosOsGrupos() throws RemoteException {
        return this.lookupAppDataNode().todosOsGrupos();
    }
    
    public ServiceAppDataNode lookupAppDataNode() throws RemoteException {
        try {
            Registry serverAppRegistry = LocateRegistry.getRegistry("localhost", 8082);
            ServiceAppDataNode serviceAppDataNode = (ServiceAppDataNode) serverAppRegistry.lookup("AppDataNode");
            return serviceAppDataNode;
        } catch (NotBoundException nbe) {
            throw new RemoteException("AppDataNode Indisponivel!");
        }
    }


}
