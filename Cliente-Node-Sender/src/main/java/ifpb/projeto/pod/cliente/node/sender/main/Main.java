/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.cliente.node.sender.main;

import ifpb.projeto.pod.cliente.node.sender.ServiceClienteNodeSenderImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;

/**
 *
 * @author kaique
 */
public class Main {

    private static Integer idMensagem = 0;

    public static void main(String[] args) throws RemoteException, AlreadyBoundException, IOException, InterruptedException {

        ServerSocket listener = new ServerSocket(8083);
        System.out.println("Cliente-Node-Sender Online");
        try {
            while (true) {
                Socket socket = listener.accept();
                try {

//                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
//                    out.println(new Date().toString());
//                    System.out.println("Conectou e mandou a resposta");
                    InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
                    char[] cbuf = new char[1024];
                    inputStreamReader.read(cbuf, 0, 1024);
                    String mensagem = new String(cbuf);

                    //Cod 1 Solicitação de Login
                    if (mensagem.length() > 0) {
                        String[] split = mensagem.split(";");
                        if (split[0].equals("1")) {
                            String resultLogin = new ServiceClienteNodeSenderImpl().loginDeUsuario(split[1], split[2]);
                            if (resultLogin.length() > 0) {
                                new PrintWriter(socket.getOutputStream(), true).println(1111);
                                System.out.println("1111");
                            } else {
                                new PrintWriter(socket.getOutputStream(), true).println(0);
                                System.out.println("0");
                            }

                            //Cod 2 Solicitação de Registro de Usuario    
                        } else if (split[0].equals("2")) {
                            boolean resultadoRegistro = new ServiceClienteNodeSenderImpl().registroDeUsuario(split[1], split[2]);
                            if (resultadoRegistro) {
                                new PrintWriter(socket.getOutputStream(), true).println(1111);
                                System.out.println("1111");
                            } else {
                                new PrintWriter(socket.getOutputStream(), true).println(0);
                                System.out.println("0");
                            }

                            //Cod 3 Solicitação de Envio de Mensagem
                        } else if (split[0].equals("3")) {
                            String[] dadosMensagens = mensagem.split(";");
                            String msg = split[1];
                            String emailUser = split[2];
                            String idGrupo = split[3];

                            String mensagemFormatada = idMensagem++ + ";" + emailUser + ";" + new Date().toString() + ";" + msg + ";" + idGrupo;
                            boolean envioDeMensagem = new ServiceClienteNodeSenderImpl().envioDeMensagem(mensagemFormatada);

                            new PrintWriter(socket.getOutputStream(), true).println(1111);
                            System.out.println("1111");
                            //Cod 4 Solicitação de busca de todos os Grupos
                        } else if (split[0].equals("4")) {
                            List<String> todosOsGrupos = new ServiceClienteNodeSenderImpl().todosOsGrupos();
                            new PrintWriter(socket.getOutputStream(), true).print(todosOsGrupos);
                            System.out.println("Enviou");
                        }
                    } else {
                        System.out.println("Vazio");
                    }
                    inputStreamReader.close();
                } finally {
                    socket.close();
                }
            }
        } finally {
            listener.close();
        }

//        // Consumindo arquivo de novos cadastros
//        while (true) {
//            File solicitacoesDeRegistro = new File("D:\\IFPB\\5.Periodo\\POD\\Projeto_Chat\\Cliente-Node-Sender\\SolicitacoesDeRegistro\\SolicitacoesDeRegistro.txt");
//            if (solicitacoesDeRegistro.exists()) {
//                FileReader reader = new FileReader(solicitacoesDeRegistro);
//                //leitor do arquivo
//                BufferedReader leitor = new BufferedReader(reader);
//                while (true) {
//                    String linha = leitor.readLine();
//                    if (linha != null) {
//
//                        String[] split = linha.split(";");
//                        String email = split[0];
//                        String senha = split[1];
//
//                        System.out.println("Registrando E-mail: " + email);
//                        ServiceClienteNodeSender nodeSender = new ServiceClienteNodeSenderImpl();
//                        nodeSender.registroDeUsuario(email, senha);
//                    } else {
//                        break;
//                    }
//                }
//                reader.close();
//                leitor.close();
//            } else {
//                System.out.println("O arquivo Não Existe");
//            }
//            Thread.sleep(3000);
//        }
    }
}
