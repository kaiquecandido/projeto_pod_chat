/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.cliente.node.sender;

import ifpb.projeto.pod.server.node.reciver.ServiceServerNodeReciver;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author kaique
 */
public class ServiceClienteNodeSenderImpl extends UnicastRemoteObject implements ServiceClienteNodeSender {

    public ServiceClienteNodeSenderImpl() throws RemoteException {
        super();
    }

    @Override
    public boolean registroDeUsuario(String email, String senha) throws RemoteException {
        return this.lookupServerNodeReciver().registroDeUsuario(email, senha);
    }

    @Override
    public String loginDeUsuario(String email, String senha) throws RemoteException {
        return this.lookupServerNodeReciver().loginDeUsuario(email, senha);
    }

    @Override
    public boolean envioDeMensagem(String mensagem) throws RemoteException {
        return this.lookupServerNodeReciver().envioDeMensagem(mensagem);
    }

    @Override
    public List<String> todosOsGrupos() throws RemoteException {
        return this.lookupServerNodeReciver().todosOsGrupos();
    }

    public ServiceServerNodeReciver lookupServerNodeReciver() throws RemoteException {
        try {
            Registry serverAppRegistry = LocateRegistry.getRegistry("localhost", 8081);
            ServiceServerNodeReciver serviceServerNodeReciver = (ServiceServerNodeReciver) serverAppRegistry.lookup("ServerNodeReciver");
            return serviceServerNodeReciver;
        } catch (NotBoundException nbe) {
            throw new RemoteException("ServiceServerNodeReciver Indisponivel!");
        }
    }

}
