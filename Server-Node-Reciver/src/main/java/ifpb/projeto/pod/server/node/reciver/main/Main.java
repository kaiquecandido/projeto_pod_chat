/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.server.node.reciver.main;

import ifpb.projeto.pod.server.node.reciver.ServiceReciverImpl;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author kaique
 */
public class Main {

    public static void main(String[] args) throws RemoteException, AlreadyBoundException {

        Registry registry = LocateRegistry.createRegistry(8081);
        registry.bind("ServerNodeReciver", new ServiceReciverImpl());
        System.out.println("Server-Node-Reciver Online!");
        
    }
}
