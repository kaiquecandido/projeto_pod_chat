/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.projeto.pod.server.node.reciver;

import ifpb.projeto.pod.server.node.ServerNode;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author kaique
 */
public class ServiceReciverImpl extends UnicastRemoteObject implements ServiceServerNodeReciver {

    public ServiceReciverImpl() throws RemoteException {
        super();
    }

    @Override
    public boolean registroDeUsuario(String email, String senha) throws RemoteException {
        return this.lookupServerNode().registroDeUsuario(email, senha);
    }

    @Override
    public String loginDeUsuario(String email, String senha) throws RemoteException {
        return this.lookupServerNode().loginDeUsuario(email, senha);
    }

    @Override
    public boolean envioDeMensagem(String mensagem) throws RemoteException {
        return this.lookupServerNode().envioDeMensagem(mensagem);
    }

    @Override
    public List<String> todosOsGrupos() throws RemoteException {
        return this.lookupServerNode().todosOsGrupos();
    }

    public ServerNode lookupServerNode() throws RemoteException {
        try {
            Registry serverAppRegistry = LocateRegistry.getRegistry("localhost", 8080);
            ServerNode serverNode = (ServerNode) serverAppRegistry.lookup("ServerNode");
            return serverNode;
        } catch (NotBoundException nbe) {
            throw new RemoteException("ServerNode Indisponivel!");
        }
    }

}
